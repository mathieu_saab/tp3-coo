package domain;

public class Bureau {
	private int idBureau;
	private String description;
	
	public Bureau(int idBureau, String description) {
		this.idBureau = idBureau;
		this.description = description;
	}

	public int getIdBureau() {
		return idBureau;
	}

	public void setIdBureau(int idBureau) {
		this.idBureau = idBureau;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
