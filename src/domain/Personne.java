package domain;

public class Personne {
	private int id, bureauId;
	private String nom;
	private String numeroTelephone;
	
	public Personne(int id, String nom) {
		super();
		this.id = id;
		this.nom = nom;
	}
	
	public Personne(int id, int bureauId, String nom) {
		super();
		this.id = id;
		this.bureauId = bureauId;
		this.nom = nom;
	}
	
	public Personne(int id, int bureauId, String nom, String numeroTelephone) {
		super();
		this.id = id;
		this.bureauId = bureauId;
		this.nom = nom;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getBureauId() {
		return bureauId;
	}
	public void setBureauId(int bureauId) {
		this.bureauId = bureauId;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getNumeroTelephone() {
		return numeroTelephone;
	}

	public void setNumeroTelephone(String numeroTelephone) {
		this.numeroTelephone = numeroTelephone;
	}
	
	
}
