package domain;

public class Chercheur extends Personne {
	private String domaine;

	public Chercheur(int id, int idBureau, String nom, String domaine) {
		super(id, idBureau, nom);
		this.domaine = domaine;
	}
	
	public Chercheur(int id, String nom, String domaine) {
		super(id, nom);
		this.domaine = domaine;
	}
	
	public Chercheur(int id, String nom, int bureau, String domaine, String numeroTelephone) {
		super(id, bureau, nom, numeroTelephone);
		this.domaine = domaine;
	}

	public String getDomaine() {
		return domaine;
	}

	public void setDomaine(String domaine) {
		this.domaine = domaine;
	}
}
