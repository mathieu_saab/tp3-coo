package domain;

public class Administrateur extends Personne {
	private String formation;

	public Administrateur(int id, int idBureau, String nom, String formation) {
		super(id, idBureau, nom);
		this.formation = formation;
	}
	
	public Administrateur(int id, String nom, String formation) {
		super(id, nom);
		this.formation = formation;
	}
	
	public Administrateur(int id, String nom, int bureau, String formation, String numeroTelephone) {
		super(id, bureau, nom, numeroTelephone);
		this.formation = formation;
	}

	public String getFormation() {
		return formation;
	}

	public void setFormation(String formation) {
		this.formation = formation;
	}
	
}
