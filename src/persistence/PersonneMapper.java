package persistence;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import domain.Administrateur;
import domain.Bureau;
import domain.Chercheur;
import domain.Personne;
import main.App;
import util.Constants;
import util.LogUtils;

public class PersonneMapper {
	static PersonneMapper instance = new PersonneMapper();
	int id;
	private final String TAG = "PersonneMapper";
	private final String req_INSERT_PERSONNE_BASE = "INSERT INTO Personne(id_personne, nom) values (?,?)";
	private final String req_INSERT_PERSONNE_CHERCHEUR = "INSERT INTO Chercheur(id_personne, domaine_activite) values (?,?)";
	private final String req_INSERT_PERSONNE_ADMINISTRATEUR = "INSERT INTO Administrateur(id_personne, formation) values (?,?)";
	//insert with desk : insert personne (avec id bureau) et insert numero de telephone
	//affect to desk : update idbureau et numero de telephone
	
	private PersonneMapper() {id = 0;}
	
	public static PersonneMapper getInstance() {
		return instance;
	}
	
	public boolean insertPersonne(Personne p) {
		try {
			PreparedStatement ps = App.getConnection().prepareStatement(req_INSERT_PERSONNE_BASE);
			ps.setInt(1, this.id);
			ps.setString(2, p.getNom());
			int result = ps.executeUpdate();
			if(result != 0) {
				LogUtils.log(TAG, Constants.SUCCESS, "Successful insert with parameters : " + this.id + ", " + p.getNom());
				id++;
				return true;
			} 
			LogUtils.log(TAG, Constants.ERROR, "No rows returned with parameters : " + this.id + ", " + p.getNom());
			return false;
		} catch (SQLException e) {
			LogUtils.log(TAG, Constants.ERROR, "Error while inserting Personne");
			e.printStackTrace();
			return false;
		}
		
	}	
	
	public boolean insertAdministrateur(Administrateur a) {
		try {
			PreparedStatement ps = App.getConnection().prepareStatement(req_INSERT_PERSONNE_BASE);
			ps.setInt(1, this.id);
			ps.setString(2, a.getNom());
			int result = ps.executeUpdate();
			if(result != 0) {
				LogUtils.log(TAG, Constants.SUCCESS, "Successful insert with parameters : " + this.id + ", " + a.getNom());
				ps = App.getConnection().prepareStatement(req_INSERT_PERSONNE_ADMINISTRATEUR);
				ps.setInt(1, id);
				ps.setString(2, a.getFormation());
				result = ps.executeUpdate();
				if(result != 0) {
					LogUtils.log(TAG, Constants.SUCCESS, "Successful insert with parameters : " + this.id + ", " + a.getFormation());
					id++;
					return true;
				} 
				LogUtils.log(TAG, Constants.ERROR, "No rows returned with parameters : " + this.id + ", " + a.getFormation());
				return false;
			} 
			LogUtils.log(TAG, Constants.ERROR, "No rows returned with parameters : " + this.id + ", " + a.getNom());
			return false;
		} catch (SQLException e) {
			LogUtils.log(TAG, Constants.ERROR, "Error while inserting Administratuer");
			e.printStackTrace();
			return false;
		}
	}	
	
	public boolean insertChercheur(Chercheur c) {
		try {
			PreparedStatement ps = App.getConnection().prepareStatement(req_INSERT_PERSONNE_BASE);
			ps.setInt(1, this.id);
			ps.setString(2, c.getNom());
			int result = ps.executeUpdate();
			if(result != 0) {
				LogUtils.log(TAG, Constants.SUCCESS, "Successful insert with parameters : " + this.id + ", " + c.getNom());
				ps = App.getConnection().prepareStatement(req_INSERT_PERSONNE_CHERCHEUR);
				ps.setInt(1, id);
				ps.setString(2, c.getDomaine());
				result = ps.executeUpdate();
				if(result != 0) {
					LogUtils.log(TAG, Constants.SUCCESS, "Successful insert with parameters : " + this.id + ", " + c.getDomaine());
					id++;
					return true;
				} 
				LogUtils.log(TAG, Constants.ERROR, "No rows returned with parameters : " + this.id + ", " + c.getDomaine());
				return false;
			} 
			LogUtils.log(TAG, Constants.ERROR, "No rows returned with parameters : " + this.id + ", " + c.getNom());
			return false;
		} catch (SQLException e) {
			LogUtils.log(TAG, Constants.ERROR, "Error while inserting Cherchuer");
			e.printStackTrace();
			return false;
		}
	}	
}
