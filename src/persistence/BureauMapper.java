package persistence;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import domain.Bureau;
import main.App;
import util.Constants;
import util.LogUtils;

public class BureauMapper {
	static BureauMapper instance = new BureauMapper();
	int id;
	private final String TAG = "BureauMapper";
	private final String req_INSERT_BUREAU = "INSERT INTO Bureau values (?,?)";
	
	
	private BureauMapper() {id = 0;}
	
	public static BureauMapper getInstance() {
		return instance;
	}
	
	public boolean insert(Bureau b) {
		try {
			PreparedStatement ps = App.getConnection().prepareStatement(req_INSERT_BUREAU);
			ps.setInt(1, this.id);
			ps.setString(2, b.getDescription());
			int result = ps.executeUpdate();
			if(result != 0) {
				LogUtils.log(TAG, Constants.SUCCESS, "Successful insert with parameters : " + this.id + ", " + b.getDescription());
				id++;
				return true;
			} 
			LogUtils.log(TAG, Constants.ERROR, "No rows returned with parameters : " + this.id + ", " + b.getDescription());
			return false;
		} catch (SQLException e) {
			LogUtils.log(TAG, Constants.ERROR, "Error while inserting Bureau");
			e.printStackTrace();
			return false;
		}
		
	}	
}
