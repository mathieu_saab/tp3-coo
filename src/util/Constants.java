package util;

public class Constants {
	
	public static final String ENVIRONMENT = "WEBTP"; //Change this line to "" "WEBTP" to change between localhost and webtp server
	public static final String WEBTP_URL = "jdbc:mysql://webtp.fil.univ-lille1.fr/saab";
	public static final String LOCAL_URL = "jdbc:mysql://localhost/java";

	public static final String ERROR = "ERROR";
	public static final String SUCCESS = "SUCCESS";
	public static final String INFO = "INFO";

}
