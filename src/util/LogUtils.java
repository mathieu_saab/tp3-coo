package util;

public class LogUtils {

	public static void log(String tag, String type, String message) {
		System.out.println(">>" + tag + " " + type.toUpperCase() + " : " + message);
	}
}
