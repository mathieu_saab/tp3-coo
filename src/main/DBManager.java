package main;

import java.sql.*;

import util.Constants;
import util.LogUtils;

public class DBManager {
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String TAG = "DBManager";

	static Connection connection;
	
	public DBManager() {
		
	}
	
	public boolean connect() {
		try {
			Class.forName(JDBC_DRIVER);
			if(Constants.ENVIRONMENT.equals("LOCAL")) {
				connection = DriverManager.getConnection(Constants.LOCAL_URL, "user", "passsword");
			} else if(Constants.ENVIRONMENT.equals("WEBTP")){
				connection = DriverManager.getConnection(Constants.WEBTP_URL, "user", "password");
			} else {
				LogUtils.log(this.TAG, Constants.ERROR, "Unknown Environment");
				return false;
			}
			LogUtils.log(this.TAG, Constants.SUCCESS, "Connection successful");
			return true;
		} catch (Exception e) {
			LogUtils.log(this.TAG, Constants.ERROR, "Connection to database failed");
			e.printStackTrace();
			return false;
		}
	}
	
	public Connection getConnection() {
		return this.connection;
	}
	
	public boolean disconnect(boolean commit) {
		if(commit) {
			try {
				connection.commit();
				connection.close();
				return true;
			} catch (SQLException e) {
				LogUtils.log(this.TAG, Constants.ERROR, "Unable to explicitly commit and close the connection");
				return false;
			}
		} else {
			try {
				connection.rollback();
				connection.close();
				return true;
			} catch (SQLException e) {
				LogUtils.log(this.TAG, Constants.ERROR, "Unable to explicitly rollback and close the connection");
				return false;
			}
		}
	}
}
