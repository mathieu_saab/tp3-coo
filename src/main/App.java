package main;

import java.sql.Connection;

import util.Constants;
import util.LogUtils;

public class App {
	//TODO : use a Visitor design pattern for PersonneMapper
	//TODO : manage rollbacks and commits
	public static String TAG = "App";
	private static DBManager session;
	private App() {}
	
	private static App instance = new App();
	
	public static App getInstance() {
		return instance;
	}
	
	public static Connection createSession() {
		LogUtils.log(TAG, Constants.INFO, "Creating app database session");
		session = new DBManager();
		session.connect();
		return session.getConnection();
	}
	
	public static Connection getConnection() {
		if(session == null) {
			return createSession();
		} else {
			return session.getConnection();
		}
	}
}
