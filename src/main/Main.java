package main;

import domain.Administrateur;
import domain.Bureau;
import domain.Chercheur;
import domain.Personne;
import persistence.BureauMapper;
import persistence.PersonneMapper;

public class Main {

	public static void main(String[] args) {
		PersonneMapper mapper = PersonneMapper.getInstance();
		
		Administrateur a = new Administrateur(0, "Test 1", "MIAGE");
		Chercheur c = new Chercheur(0, "Hervé Blanchon", "Traduction de langues automatique");
		mapper.insertAdministrateur(a);
		mapper.insertChercheur(c);
	}

}
